#include <AsgTools/MessageCheck.h>
#include <MyAnalysis/MyxAODAnalysis.h>



MyxAODAnalysis :: MyxAODAnalysis (const std::string& name,
                                  ISvcLocator *pSvcLocator)
    : EL::AnaAlgorithm (name, pSvcLocator)
{
  // Here you put any code for the base initialization of variables,
  // e.g. initialize all pointers to 0.  This is also where you
  // declare all properties for your algorithm.  Note that things like
  // resetting statistics variables or booking histograms should
  // rather go into the initialize() function.
}



StatusCode MyxAODAnalysis :: initialize ()
{
  // Here you do everything that needs to be done at the very
  // beginning on each worker node, e.g. create histograms and output
  // trees.  This method gets called before any input files are
  // connected.

  ANA_MSG_INFO ("in initialize");

  ANA_CHECK (book (TTree ("analysis", "My analysis ntuple")));
  TTree* mytree = tree ("analysis");
  mytree->Branch ("RunNumber", &m_runNumber);
  mytree->Branch ("EventNumber", &m_eventNumber);
  m_jetEta = new std::vector<float>();
  mytree->Branch ("JetEta", &m_jetEta);
  m_jetPhi = new std::vector<float>();
  mytree->Branch ("JetPhi", &m_jetPhi);
  m_jetPt = new std::vector<float>();
  mytree->Branch ("JetPt", &m_jetPt);
  m_jetE = new std::vector<float>();
  mytree->Branch ("JetE", &m_jetE);

  ANA_CHECK(book(TH1F("h_jetPt","h_jetPt",100,0,500))); //jet pt [GeV]
  ANA_CHECK(book(TH1F("h_MDTdEdx", ";MDT dE/dx;Number of muons", 600, -100., 500.)));

  return StatusCode::SUCCESS;
}



StatusCode MyxAODAnalysis :: execute ()
{
  // Here you do everything that needs to be done on every single
  // events, e.g. read input variables, apply cuts, and fill
  // histograms and trees.  This is where most of your actual analysis
  // code will go.

  ANA_MSG_INFO ("in execute");

  // retrieve the eventInfo object from the event store
  const xAOD::EventInfo *eventInfo = nullptr;
  ANA_CHECK (evtStore()->retrieve (eventInfo, "EventInfo"));
  m_runNumber = eventInfo->runNumber ();
  m_eventNumber = eventInfo->eventNumber ();

  // print out run and event number from retrieved object
  ANA_MSG_INFO ("in execute, runNumber = " << eventInfo->runNumber() << ", eventNumber = " << eventInfo->eventNumber());

  //loop over the muons in the container
  const xAOD::MuonContainer* muons = nullptr;
  ANA_CHECK(evtStore()->retrieve(muons, "Muons"));

// create a shallow copy of the muons container
  auto muons_shallowCopy = xAOD::shallowCopyContainer(*muons);
  std::unique_ptr<xAOD::MuonContainer> muonsSC(muons_shallowCopy.first);
  std::unique_ptr<xAOD::ShallowAuxContainer> muonsAuxSC(muons_shallowCopy.second);

  for (auto muonSC : *muonsSC) // this is not the loop over the original container, it's the loop over the shallow-copy container
  {
     hist("h_MDTdEdx")->Fill(muonSC->auxdata<float>("meanDeltaADCCountsMDT"), 1);
  }

  //loop over the jets in the container
  const xAOD::JetContainer* jets = nullptr;
  ANA_CHECK (evtStore()->retrieve (jets, "AntiKt4EMTopoJets"));
  m_jetEta->clear();
  m_jetPhi->clear();
  m_jetPt->clear();
  m_jetE->clear();
  for (const xAOD::Jet* jet : *jets) {
    hist ("h_jetPt")->Fill (jet->pt() * 0.001); // GeV
    m_jetEta->push_back (jet->eta ());
    m_jetPhi->push_back (jet->phi ());
    m_jetPt-> push_back (jet->pt ());
    m_jetE->  push_back (jet->e ());
  } // end for loop over jets


  tree("analysis")->Fill();

  return StatusCode::SUCCESS;
}



StatusCode MyxAODAnalysis :: finalize ()
{
  // This method is the mirror image of initialize(), meaning it gets
  // called after the last event has been processed on the worker node
  // and allows you to finish up any objects you created in
  // initialize() before they are written to disk.  This is actually
  // fairly rare, since this happens separately for each worker node.
  // Most of the time you want to do your post-processing on the
  // submission node after all your histogram outputs have been
  // merged.

  delete m_jetEta;
  delete m_jetPhi;
  delete m_jetPt;
  delete m_jetE;

  return StatusCode::SUCCESS;
}
